# H2S File Checker #

* A file checking test framework for 42's H2S program

### What is this repository for? ###

* Development of tests for a Hack High School project

### How do I write tests? ###

* Clone the repo
* Read this README
* Edit `<assignment>_test_cases.json`, using the examples as a guide
* Edit `callbacks.py` using the examples as a guide

### Usage ###
* `git clone` this directory if you haven't already
* (Optional) add this to your profile and restart your terminal
```
function h2sfc {
    cwd=$(pwd)
    cd ~/path/to/cloned/h2sfilechecker
    python checker.py $1 $2
    cd $cwd
}
```
* If you did the step above:
	- Run `h2sfc` or `h2sfc <test> <path>`
* OR...
	- `cd` to the cloned directory
	- Run `python checker.py` or `python checker.py <test> <path>`
* Run `python checker -h` or `h2sfc -h` for _help_

### Tests ###
* There are 9 types of tests (and they are run in this order):
	1. directory
	2. import
	3. class init
	4. instance variable
	5. class variable
	6. instance method
	7. class method
	8. function
	9. main
* Each test will:
	- Check for the existence of the item
	- Test the specified item (execution, import, etc...)
	- Create a global variable storing the success of the test
	- Call a callback function (except directory and import tests)
	- Return True if both the test and the callback succeeds, otherwise, return False
	- NOTE: standard output is supressed for testing purposes. You can capture and process stdout using `capture_stdout()` and `read_captured_stdout()` _(see Callbacks)_

### Global Success Variables ###
* Each type of test will create a global variable storing the success of the test
* For example, where <dm> is the directory or module name:
	- `<dm>_directory_success`
	- `<dm>_<module>_import_success`
	- `<dm>_<class_name>_class_success`
	- `<dm>_<class_name>_<ivariable>_ivar_success`
	- `<dm>_<class_name>_<imethod>_imethod_success`
	- `<dm>_<class_name>_<cvariable>_cvar_success`
	- `<dm>_<class_name>_<cmethod>_cmethod_success`
	- `<dm>_<function_name>_function_success`
	- `<dm>_main_success`
* These variables should be used as prerequisites for further testing
* NOTE: <class_name> will be the lowercase equivalent of the ClassName (not camel_case).
* Ex: `SomeClass` will produce `someclass_class_success` instead of `some_class_class_success`

### Objects As Arguments For Tests ###
* A test may require objects as arguments for testing
* To accomplish this, pass in the string _"make_object"_ to the _args_ list
* After the string, add the _classname_ then the _arguments_, if any, for the constructor
* The string and it's arguments must go at the end of the _args_ list
* `"args":["OtherArgs", "make_object", "SomeClass", "objarg1", 2, "objarg3", "make_object", "SomeOtherClass"]`

### Callbacks ###
* Callback functions are custom functions called at the end of a test case
* Callback functions return a boolean on success or failure
* Callback functions are where many custom tests can be written
* Callback functions can be used to test edge cases, state change, etc...
* Callback functions have access to the same success variables
	- This means a callback can check if prerequisites exist
	- This allows for data mocking if and only if dependent functionality is implemented first
* Callbacks can use local imports for testing a module (prereqs can prevent the callback from running if the module has an error)
```
def callback():
	from localmodule import Foo
	s = Foo()
```
* To capture and test standard output in a callback:
```
def callback():
	with capture_stdout() as out:
		#produce output
		print("expected")
	with read_captured_stdout(out) as captured:
		#take care with trailing newlines in Python's print
		expected_output = "expected\n"
		return captured == expected_output
```

### Directory Tests ###
* `"directories":[]`

### Import Tests ###
* `"import":[]`
* `"module"` (required)
* `"import"` (optional)
* `"directory"` (optional)
* `"prereq_bool"` (optional)

### Class Initializer Tests ###
* `"class":[]`
* `"class"` (required)
* `"args"` (optional)
* `"prereq_bool"` (optional)
* `"callback_func"` (optional)
* `"callback_args"` (optional)
* `"callback_msg"` (optional)

### Instance Variable Tests ###
* `"ivar":[]`
* `"class"` (required)
* `"var"` (required)
* `"init_params"` (optional)
* `"prereq_bool"` (optional)
* `"callback_func"` (optional)
* `"callback_args"` (optional)
* `"callback_msg"` (optional)

### Class Variable Tests ###
* `"cvar":[]`
* `"class"` (required)
* `"var"` (required)
* `"prereq_bool"` (optional)
* `"callback_func"` (optional)
* `"callback_args"` (optional)
* `"callback_msg"` (optional)

### Instance Method Tests ###
* `"imethod":[]`
* `"class"` (required)
* `"method"` (required)
* `"args"` (optional)
* `"init_params"` (optional)
* `"prereq_bool"` (optional)
* `"callback_func"` (optional)
* `"callback_args"` (optional)
* `"callback_msg"` (optional)

### Class Method Tests ###
* `"cmethod":[]`
* `"class"` (required)
* `"method"` (required)
* `"args"` (optional)
* `"init_params"` (optional)
* `"prereq_bool"` (optional)
* `"callback_func"` (optional)
* `"callback_args"` (optional)
* `"callback_msg"` (optional)

### Function Tests ###
* `"function":[]`
* `"function"` (required)
* `"args"` (optional)
* `"prereq_bool"` (optional)
* `"callback_func"` (optional)
* `"callback_args"` (optional)
* `"callback_msg"` (optional)

### Main Tests ###
* `"main":[]`
* `"directory"` (optional)
* `"args"` (optional)
* `"prereq_bool"` (optional)
* `"callback_func"` (optional)
* `"callback_args"` (optional)
* `"callback_msg"` (optional)

### Params ###
* `"args"`
	- _list_
	- Arguments can be of type `string`, `int`, `float`, `boolean`, or `null`.
	- The arguments are passed into the function/constructor/method in the order they are given.
* `"callback_args"`
	- _list_
	- Parameters to pass into the callback function if/when it is called
* `"callback_func"`
	- _string_
	- Function that will be called on successful completion of the basic test
* `"callback_msg"`
	- _string_
	- A message to be displayed if the callback function fails
* `"class"`
	- _string_
	- The class name
* `"directory"`
	- _string_
	- The subdirectory name in which the files to be tested are contained
* `"function"`
	- _string_
	- The function name
* `"import"`
	- _list of strings_
	- Items to be imported from the module
* `"init_params"`
	- _list_
	- Parameters to be passed into a class initializer
* `"method"`
	- _string_
	- The method name
* `"module"`
	- _string_
	- The module name
* `"namespace"`
	- _string_
	- Function namespace
	- Usage is highly recommended, as it prevents naming conflicts
* `"prereq_bool"`
	- _list of booleans (as strings)_
	- Ex: `<class>_class_success` should be a prerequisite of testing the class' methods or variables
* `"var"`
	- _string_
	- The class/instance variable name