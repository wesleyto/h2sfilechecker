from checker import capture_stdout, read_captured_stdout, to_class_name

#########################################################
#                 Add Your Callbacks Below              #
#           Delete Example Tests and Callbacks          #
#########################################################

#########################################################
#              CLASS INITIALIZER CALLBACKS              #
#########################################################

def first_class_callback():
	from first_class import FirstClass
	with capture_stdout() as out:
		FirstClass()
	with read_captured_stdout(out) as captured:
		expected_output = "First!\n"
		return captured == expected_output

def animal_callback(module):
	from animal import Animal
	class_name = to_class_name(module)
	exec("from {} import {}".format(module, class_name))
	p = eval(class_name)
	return issubclass(p, Animal)

def animal_init():
	from animal import Animal
	with capture_stdout() as out:
		Animal()
	with read_captured_stdout(out) as captured:
		expected = "Making An Animal\n"
		return captured == expected

def player_callback(module):
	from player import Player
	class_name = to_class_name(module)
	exec("from {} import {}".format(module, class_name))
	p = eval(class_name)
	return issubclass(p, Player)

def person_callback(module):
	from person import Person
	class_name = to_class_name(module)
	exec("from {} import {}".format(module, class_name))
	p = eval(class_name)
	return issubclass(p, Person)

#########################################################
#              INSTANCE VARIABLE CALLBACKS              #
#########################################################

def cellphone_test(f, s):
	from cellphone import Cellphone
	c = Cellphone()
	with capture_stdout() as out:
		getattr(c, f)()
	with read_captured_stdout(out) as captured:
		expected_output = "{}\n".format(s)
		return captured == expected_output

def animal_sleep(module):
	exec("from {} import {}".format(module, to_class_name(module)))
	a = eval(to_class_name(module))()
	with capture_stdout() as out:
		a.sleep()
	with read_captured_stdout(out) as captured:
		expected_output = "Zzz...\n"
		return captured == expected_output

def animal_speak(module, phrase):
	exec("from {} import {}".format(module, to_class_name(module)))
	a = eval(to_class_name(module))()
	with capture_stdout() as out:
		a.speak()
	with read_captured_stdout(out) as captured:
		expected_output = "{}\n".format(phrase) if phrase else ""
		return captured == expected_output

def ex03_speak(module, name, s):
	exec("from {} import {}".format(module, to_class_name(module)))
	g = eval(to_class_name(module))()
	with capture_stdout() as out:
		g.speak(name)
	with read_captured_stdout(out) as captured:
		expected_output = "{}\n".format(s)
		return captured == expected_output

def building_name(name, capacity):
	from building import Building
	b = Building(name, capacity)
	return b.name == name

def building_capacity(name, capacity):
	from building import Building
	b = Building(name, capacity)
	return b.capacity == capacity

def campus_buildings():
	from campus import Campus
	c = Campus()
	return c.buildings == []

def campus_num_buildings():
	from campus import Campus
	c = Campus()
	return c.num_buildings == 0

def campus_capacity():
	from campus import Campus
	c = Campus()
	return c.capacity == 0

def classroom_teacher():
	from classroom import Classroom
	from teacher import Teacher
	c = Classroom("Teacher")
	try:
		assert type(c.teacher).__name__ == "Teacher"
		assert c.teacher.name == "Teacher"
		return True
	except:
		return False

def classroom_students():
	from classroom import Classroom
	c = Classroom("Teacher")
	try:
		assert c.students == []
		return True
	except:
		return False

def classroom_capacity():
	from classroom import Classroom
	c = Classroom("Teacher")
	return c.capacity == 20

#########################################################
#                CLASS VARIABLE CALLBACKS               #
#########################################################

def player_playbook():
	from player import Player
	return len(Player.playbook) > 0

#########################################################
#               INSTANCE METHOD CALLBACKS               #
#########################################################

def animal_output(module):
	from animal import Animal
	class_name = to_class_name(module)
	exec("from {} import {}".format(module, class_name))
	with capture_stdout() as out:
		eval(class_name)()
	with read_captured_stdout(out) as captured:
		expected = "Making A {}\n".format(module.capitalize())
		return captured == expected

def building_get_info(name, capacity):
	from building import Building
	b = Building(name, capacity)
	with capture_stdout() as out:
		b.get_info()
	with read_captured_stdout(out) as captured:
		expected = "Building \"Math Building\" can hold 25 people\n"
		return captured == expected

def campus_add_building():
	from building import Building
	from campus import Campus
	status = True
	c = Campus()
	b1 = Building("name", 25)
	c.add_building(b1)
	status = status and c.buildings[0] == b1 and c.num_buildings == 1 and c.capacity == 25
	b2 = Building("name", 17)
	c.add_building(b2)
	status = status and c.buildings[1] == b2 and c.num_buildings == 2 and c.capacity == 42
	return status

def campus_get_info():
	from building import Building
	from campus import Campus
	c = Campus()
	b1 = Building("name", 25)
	b2 = Building("name", 17)
	c.add_building(b1)
	c.add_building(b2)
	with capture_stdout() as out:
		c.get_info()
	with read_captured_stdout(out) as captured:
		expected = "The campus has 2 building(s) with a combined capacity of 42 people\n"
		return captured == expected

def player_outputs(module, class_name):
	from random import randint
	c = "from {} import {}".format(module, class_name)
	print(c)
	exec(c)
	c = eval(class_name)
	p = c()
	for play in p.playbook:
		with capture_stdout() as out:
			p.run_play(play)
		with read_captured_stdout(out) as captured:
			return len(captured) > 5

def player_outputs_unknown(module, class_name):
	from random import randint
	c = "from {} import {}".format(module, class_name)
	print(c)
	exec(c)
	c = eval(class_name)
	p = c()
	with capture_stdout() as out:
		p.run_play("".join([chr(x) for x in [randint(97, 122) for i in range(randint(10, 30))]]))
	with read_captured_stdout(out) as captured:
		return len(captured) > 5

def classroom_add_student():
	from classroom import Classroom
	from random import randint
	name = "".join([chr(n+ord('a')) for n in [randint(0, 25) for i in range(randint(5, 10))]])
	c = Classroom("Teacher")
	try:
		assert len(c.students) == 0
		c.add_student(name)
		assert len(c.students) == 1
		assert c.students[0].name == name
		assert type(c.students[0]).__name__ == "Student"
		return True
	except:
		return False

def classroom_begin_class():
	from classroom import Classroom
	from random import randint
	t = "Teacher"
	def make_name():
		return "".join([chr(n+ord('a')) for n in [randint(0, 25) for i in range(randint(5, 10))]])
	c = Classroom(t)
	names = [make_name() for i in range(3)]
	for name in names:
		c.add_student(name)
	with capture_stdout() as out:
		c.begin_class()
	with read_captured_stdout(out) as captured:
		expected = "{}: Welcome to class, {}\n{}: Good morning, {}\n" * 3
		expected = expected.format(
			t, names[0], names[0], t,
			t, names[1], names[1], t,
			t, names[2], names[2], t
		)
		return captured == expected

def classroom_status():
	from classroom import Classroom
	from random import randint
	def make_name():
		return "".join([chr(n+ord('a')) for n in [randint(0, 25) for i in range(randint(5, 10))]])
	t = make_name()
	c = Classroom(t)
	r = randint(3, 10)
	for i in range(r):
		c.add_student(make_name())
	with capture_stdout() as out:
		c.status()
	with read_captured_stdout(out) as captured:
		expected = "This classroom has a standard capacity of {} and is run by {}. It currently has {} students\n".format(Classroom.capacity, t, r)
		return captured == expected

def dad_speak():
	from dad import Dad
	d = Dad()
	with capture_stdout() as out:
		d.speak()
	with read_captured_stdout(out) as captured:
		arr = [87, 104, 97, 116, 32, 105, 115, 32, 116, 104, 101, 32, 111,
			98, 106, 101, 99, 116, 45, 111, 114, 105, 101, 110, 116, 101,
			100, 32, 119, 97, 121, 32, 111, 102, 32, 98, 101, 99, 111, 109,
			105, 110, 103, 32, 119, 101, 97, 108, 116, 104, 121, 63]
		expected = "{}\n".format(''.join([chr(n) for n in arr]))
		return captured == expected

def joke_reply():
	from joke import Joke
	j = Joke()
	with capture_stdout() as out:
		j.reply()
	with read_captured_stdout(out) as captured:
		arr = [73, 110, 104, 101, 114, 105, 116, 97, 110, 99, 101]
		expected = "{}\n".format(''.join([chr(n) for n in arr]))
		return captured == expected

#########################################################
#                 CLASS METHOD CALLBACKS                #
#########################################################

#########################################################
#                   FUNCTION CALLBACKS                  #
#########################################################

#########################################################
#                     MAIN CALLBACKS                    #
#########################################################

def oop_ex00_main():
	from ex00.main import main as ex00main
	with capture_stdout() as out:
		ex00main()
	with read_captured_stdout(out) as captured:
		expected = "First!\n"
		return captured == expected

def oop_ex01_main():
	from ex01.main import main as ex01main
	with capture_stdout() as out:
		ex01main()
	with read_captured_stdout(out) as captured:
		expected = "Making A Call...\nSending A Text...\nCRASH! CRACK! ...\n"
		return captured == expected

def oop_ex02_main():
	from ex02.main import main as ex02main
	with capture_stdout() as out:
		ex02main()
	with read_captured_stdout(out) as captured:
		expected = "Making An Animal\nMaking A Dog\nMaking A Cat\nWoof\nMeow\nZzz...\nZzz...\nZzz...\n"
		return captured == expected

def oop_ex03_main():
	from ex03.main import main as ex03main
	with capture_stdout() as out:
		ex03main()
	with read_captured_stdout(out) as captured:
		expected = "Hello Wesley\nI'd say it's good to see you Wesley, but I'd be lying...\n"
		return captured == expected

def oop_ex04_main():
	from ex04.main import main as ex04main
	with capture_stdout() as out:
		ex04main()
	with read_captured_stdout(out) as captured:
		expected = "Building \"Math Building\" can hold 25 people\nBuilding \"Science Building\" can hold 17 people\nThe campus has 2 building(s) with a combined capacity of 42 people\n"
		return captured == expected

def oop_ex05_main():
	from ex05.main import main as ex05main
	with capture_stdout() as out:
		ex05main()
	with read_captured_stdout(out) as captured:
		return len(captured) > 10

def oop_ex06_main():
	from ex06.main import main as ex06main
	with capture_stdout() as out:
		ex06main()
	with read_captured_stdout(out) as captured:
		expected = ("This classroom has a standard capacity of 20 " +
		"and is run by Wesley. It currently has 3 students\nThis " +
		"classroom has a standard capacity of 20 and is run by Anand. " +
		"It currently has 2 students\nWesley: Welcome to class, Alice\n" +
		"Alice: Good morning, Wesley\nWesley: Welcome to class, Carol\n" +
		"Carol: Good morning, Wesley\nWesley: Welcome to class, Eve\n" +
		"Eve: Good morning, Wesley\nAnand: Welcome to class, Bob\n" +
		"Bob: Good morning, Anand\nAnand: Welcome to class, Dave\n" +
		"Dave: Good morning, Anand\n")
		return captured == expected

def oop_ex07_main():
	from ex07.main import main as ex07main
	with capture_stdout() as out:
		ex07main()
	with read_captured_stdout(out) as captured:
		arr = [87, 104, 97, 116, 32, 105, 115, 32, 116, 104, 101,
			32, 111, 98, 106, 101, 99, 116, 45, 111, 114, 105,
			101, 110, 116, 101, 100, 32, 119, 97, 121, 32, 111,
			102, 32, 98, 101, 99, 111, 109, 105, 110, 103, 32,
			119, 101, 97, 108, 116, 104, 121, 63, 10, 73, 110,
			104, 101, 114, 105, 116, 97, 110, 99, 101, 10]
		expected = ''.join([chr(n) for n in arr])
		return captured == expected