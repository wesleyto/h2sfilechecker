import inspect, sys, os, json, shutil
from functools import wraps, reduce
from random import randint
from contextlib import contextmanager
from callbacks import *
from pathlib import Path

#########################################################
#                         COLORS                        #
#########################################################

MGNT = SUBHEADER = '\033[95m'
GREEN = OKGREEN = '\033[92m'
BLUE = OKBLUE = '\033[36m'
HEADER = '\033[1;95m'
YLW = WARNING = '\033[93m'
FAIL = '\033[91m'
ENDC = '\033[0m'
LOGO = '\033[1;36m'

#########################################################
#                   PRINT HELPER VARS                   #
#########################################################

WIDTH = 80
BORDER_CHAR = "#"
BORDER = BORDER_CHAR * 2
WIDTH_A = WIDTH - 2 * len(BORDER)
HDR_STR = BORDER_CHAR * WIDTH + "\n" + BORDER + "{:^{_w}}" + BORDER + "\n" + BORDER_CHAR * WIDTH
SBHDR_STR = SUBHEADER + BORDER + "{}{:^{_w}}" + ENDC + SUBHEADER + BORDER
LOGOLINE_STR = LOGO + BORDER + "{}{:^{_w}}" + ENDC + LOGO + BORDER
ERR_SBHDR_STR = SBHDR_STR.format(FAIL, "Error", _w=WIDTH_A)
MODULE_ERR_STR = "Module '{}' could not be imported"
FILE_ERR_STR = "File '{}.py' could not be found"
NDEF_ERR_STR = "Module '{}' does not contain '{}'"
MODULE_OK_STR = "All '{}' Module tests OK"
NO_MTHD_ERR_STR = "'{}.{}' is not implemented"
NO_FUNC_ERR_STR = "'{}()' is not implemented"
MTHD_ARGS_ERR_STR = "'{}.{}' args error: {}"
FUNC_ARGS_ERR_STR = "'{}()' args error: {}"
FUNC_ERR_STR = "'{}()' error: {}"
CMTHD_ERR_STR = "'{}.{}' is not @classmethod"
NO_IVAR_ERR_STR = "No Inst. Variable '{}' in Class '{}'"
NO_CVAR_ERR_STR = "No Class Variable '{}' in Class '{}'"
CVAR_IS_IVAR_ERR_STR = "'{}.{}' is an instance variable"
IVAR_IS_CVAR_ERR_STR = "'{}.{}' is a class variable"
ATTR_OK_STR = "{}.{} is OK"
FUNC_OK_STR = "{}() is OK"
CLASS_OK_STR = "Class '{}' initializer is OK"
IMPORT_ERR_STR = "Module {}: {}"
INIT_ARG_ERR_STR = "'{}' init: Constructor requires {} extra parameters"
INIT_ERR_STR = "'{}' init: {}"
INIT_SELF_ERR_STR = "'{}' init: Constructor missing a 'self' parameter"
SUCCESS_STR = "({}/{}) {} Tests [{} skipped]"
WARN_STR = "({}/{}) {} Tests [{} skipped]"
FAILURE_STR = "({}/{}) {} Tests [{} skipped]"
PREREQ_STR = "{}: Prerequisite not met for testing"
ALL_PASSED_STR = "All Tests Passed!"
NO_TESTS_STR = "No Tests To Run"
DIR_STR = "'{}' directory found"
DIR_NFOUND_STR = "'{}' directory not found"
MAIN_ERR_STR = "'{}main': {}"
MAIN_OK_STR = "'{}' is OK"

#########################################################
#                    STATUS TRACKERS                    #
#########################################################
#[passed, ran, skipped]
DTESTS,ITESTS,CTESTS,FTESTS,MTESTS = [0,0,0],[0,0,0],[0,0,0],[0,0,0],[0,0,0]
IVTESTS,CVTESTS,IMTESTS,CMTESTS = [0,0,0],[0,0,0],[0,0,0],[0,0,0]

#########################################################
#                     MISC GLOBALS                      #
#########################################################

SUCCESS, FAILURE = True, False
TEST_CASE = ""
PROJECT_PATH = "."
TEMPNAME = "temp_"+''.join([chr(n) for n in [randint(ord('a'), ord('z')) for x in range(10)]])
WHITELIST = ["checker.py", "README.md", "callbacks.py", "test_cases.json", ".git",
	"paths.json", "oop_test_cases.json", "sq_test_cases.json", "ll_test_cases.json",
	"pvnp_test_cases.json", ".gitignore"]
BLACKLIST = [".DS_Store", "__pycache__", ".git", ".gitignore"]
AND = lambda x, y: x and y
STR = lambda s: str(s) if s != '' else '""'

#########################################################
#                     PRINT HELPERS                     #
#########################################################

def style_print(style, *s):
	print(style, *s, ENDC, sep='')

def print_logoline(s):
	style_print(LOGO, BORDER, s, BORDER)

def print_logo():
	lines = [
		" _  _ ___ ___",
		"| || |_  ) __|",
		"| __ |/ /\\__",
		"|_||_/___|___/",
		"| __/ __|          HACK HIGH SCHOOL",
		"| _| (__           FILE CHECKER",
		"|_| \\___|"
	]
	s = ' ' * ((WIDTH_A - max(len(x) for x in lines)) // 2)
	style_print(LOGO, BORDER_CHAR * WIDTH)
	for line in lines:
		print_logoline("{:{_w}}".format(s + line, _w=WIDTH_A))
	print_logoline(" " * WIDTH_A)
	print_logoline("{:^{_w}}".format(TITLE, _w=WIDTH_A))
	print_logoline(" " * WIDTH_A)
	style_print(LOGO, BORDER_CHAR * WIDTH)

def print_header(*s):
	style_print(SUBHEADER, HDR_STR.format(''.join(s), _w=WIDTH_A))

def print_subheader(*s):
	style_print(SUBHEADER, BORDER + " " * WIDTH_A + BORDER)
	style_print(SBHDR_STR.format(SUBHEADER, ''.join(s), _w=WIDTH_A))

def print_fail_subheader(*s):
	style_print(SBHDR_STR.format(FAIL, ''.join(s), _w=WIDTH_A))

def print_warn_subheader(*s):
	style_print(SBHDR_STR.format(WARNING, ''.join(s), _w=WIDTH_A))

def print_ok_subheader(*s):
	style_print(SBHDR_STR.format(OKGREEN, ''.join(s), _w=WIDTH_A))

def print_begin():
	print_logo()
	print_header("TEST BEGIN")

def result_helper(d, t):
	if d[0] == d[1] and d[2] == 0:
		style_print(SBHDR_STR.format(OKGREEN, SUCCESS_STR.format(d[0], d[1], t, d[2]), _w=WIDTH_A))
	elif d[2] != 0:
		style_print(SBHDR_STR.format(WARNING, WARN_STR.format(d[0], d[1], t, d[2]), _w=WIDTH_A))			
	else:
		style_print(SBHDR_STR.format(FAIL, FAILURE_STR.format(d[0], d[1], t, d[2]), _w=WIDTH_A))

def print_results():
	from functools import reduce
	d = {
		"Directory": DTESTS,
		"Import": ITESTS,
		"Class Init": CTESTS,
		"Instance Variable": IVTESTS,
		"Instance Method": IMTESTS,
		"Class Variable": CVTESTS,
		"Class Method": CMTESTS,
		"Function": FTESTS,
		"Main": MTESTS
	}
	print_header("RESULTS")
	f = lambda x, y: [x[0]+y[0], x[1]+y[1], x[2]+y[2]]
	v = reduce(f, d.values())
	if not (v[0] == v[1] and v[2] == 0):
		for k in d:
			result_helper(d[k], k)
	result_helper(v, "Total Passed")

def print_end():
	print_header("TEST END")

#########################################################
#                    OUTPUT HANDLERS                    #
#########################################################

@contextmanager
def suppress_stdout():
	with open(os.devnull, "w") as devnull:
		old_stdout = sys.stdout
		sys.stdout = devnull
		try:
			yield
		finally:
			sys.stdout = old_stdout

@contextmanager
def capture_stdout():
	i = 0
	while os.path.isfile("{}{}".format(TEMPNAME, i)):
		i += 1
	with open('{}{}'.format(TEMPNAME, i), "w") as capture:
		old_stdout = sys.stdout
		sys.stdout = capture
		try:
			yield capture
		finally:
			sys.stdout = old_stdout

@contextmanager
def read_captured_stdout(textwrapper):
	with open(textwrapper.name, "r") as capture:
		try:
			yield capture.read()
		finally:
			os.remove(textwrapper.name)

def wait_for_results(f, s):
	def _decorator():
		with capture_stdout() as out:
			f()
		with read_captured_stdout(out) as captured:
			dicts = {
				"import_test_suite": ITESTS, "dir_test_suite": DTESTS,
				"class_test_suite": CTESTS, "ivar_test_suite": IVTESTS,
				"imethod_test_suite": IMTESTS, "cvar_test_suite": CVTESTS,
				"cmethod_test_suite": CMTESTS, "func_test_suite": FTESTS,
				"main_test_suite": MTESTS
			}
			d = dicts[f.__name__]
			print_subheader(s)
			if d[0] == 0 and d[1] == 0 and d[2] == 0:
				print_ok_subheader(NO_TESTS_STR)
			elif d[0] == d[1] and d[2] == 0:
				print_ok_subheader(ALL_PASSED_STR)
			else:
				print(captured.strip())
	return wraps(f)(_decorator)

#########################################################
#                      MAIN HELPERS                     #
#########################################################

def get_path(key):
	with open("paths.json", "r") as paths:
		p = json.loads(paths.read())
		return p[key]

def set_path(key, value):
	p = {}
	with open("paths.json", "r") as paths:
		p = json.loads(paths.read())
	p[key] = value
	with open("paths.json", "w") as paths:
		json.dump(p, paths)

def select_project():
	global PROJECT_PATH, TEST_CASE
	quit = False
	if (os.path.isfile("paths.json")):
		q = HEADER + "Which project do you want to check?" + ENDC
		q += "\n\t" + YLW + "[1]" + MGNT + "\tOOP Introduction"
		q += "\n\t" + YLW + "[2]" + MGNT + "\tLinked Lists"
		q += "\n\t" + YLW + "[3]" + MGNT + "\tStacks And Queues"
		q += "\n\t" + YLW + "[4]" + MGNT + "\tPvNP"
		q += "\n\t" + YLW + "[Q]" + MGNT + "\tQuit\n\t>>>\t" + ENDC
		while True:
			ui = input(q).lower()
			if ui and ui == '1':
				TEST_CASE = 'oop_test_cases.json'
				PROJECT_PATH = get_path("oop")
				break
			elif ui and ui == '2':
				TEST_CASE = 'll_test_cases.json'
				PROJECT_PATH = get_path("ll")
				break
			elif ui and ui == '3':
				TEST_CASE = 'sq_test_cases.json'
				PROJECT_PATH = get_path("sq")
				break
			elif ui and ui == '4':
				TEST_CASE = 'pvnp_test_cases.json'
				PROJECT_PATH = get_path("pvnp")
				break
			elif ui and ui == 'q':
				quit = True
				break
		if quit:
			exit()
	else:
		raise Exception("paths.json file is missing")

def select_path():
	global PROJECT_PATH
	home = str(Path.home()) + '/'
	quit = False
	name = TEST_CASE[:TEST_CASE.index('_')]
	run = True
	q = "{}The current project path is set to\n\t{}{}{}".format(HEADER, ENDC, BLUE, PROJECT_PATH)
	q += "{}\nIs this correct?{}".format(HEADER, ENDC)
	q += "{}\n\t[Y]{}\tCorrect, proceed to testing".format(YLW, MGNT)
	q += "{}\n\t[N]{}\tNo, let me enter a new path".format(YLW, MGNT)
	q += "{}\n\t[Q]{}\tQuit".format(YLW, MGNT)
	q += "{}\n\t{}>>>\t{}".format(YLW, MGNT, ENDC)
	while run:
		ui = input(q).lower()
		if ui and ui[0] == 'n':
			while True:
				q2 = "{}What is the correct path {}{}[Q to quit]{}?{}\n\t{}"
				new_path = input(q2.format(HEADER, ENDC, YLW, HEADER, ENDC, home))
				if new_path and new_path[0].lower() == 'q':
					exit()
				new_path = home + new_path
				if os.path.isdir(new_path):
					PROJECT_PATH = new_path
					set_path(name, PROJECT_PATH)
					run = False
					break
				else:
					style_print(FAIL, "Path '{}' does not exist".format(new_path) + ENDC)
		elif ui and ui[0] == 'q':
			exit()
		elif ui and ui[0] == 'y':
			if os.path.isdir(PROJECT_PATH):
				return
			else:
				style_print(FAIL, "Path '{}' does not exist".format(PROJECT_PATH) + ENDC)

def handle_cli():
	global TEST_CASE, PROJECT_PATH
	msg = "Usage:\tpython checker.py <test> <path_to_project>\n\t" \
		"<test>:\t'1' or 'oop' for OOP Introduction\n\t\t" \
		"'2' or 'sq' for Stacks & Queues\n\t\t" \
		"'3' or 'll' for Linked Lists\n\t\t" \
		"'4' or 'pvnp' for Final Project"
	av = sys.argv
	if "-h" in av:
		style_print(YLW, msg)
		return 1
	elif len(av) == 3 or len(av) == 2:
		case = av[1].lower()
		if case in ['1', "oop"]:
			TEST_CASE = 'oop_test_cases.json'
			case = 'oop'
			PROJECT_PATH = get_path(case)
		elif case in ['2', "ll"]:
			TEST_CASE = 'll_test_cases.json'
			case = 'll'
			PROJECT_PATH = get_path(case)
		elif case in ['3', "sq"]:
			TEST_CASE = 'sq_test_cases.json'
			case = 'sq'
			PROJECT_PATH = get_path(case)
		elif case in ['4', "pvnp"]:
			TEST_CASE = 'pvnp_test_cases.json'	
			case = 'pvnp'
		else:
			style_print(FAIL, ">>> '{}' is not a valid project to test".format(av[1]))
			return 1
		PROJECT_PATH = get_path(case)
		if len(av) == 2:
			return 4
		if os.path.isdir(av[2]):
			PROJECT_PATH = av[2]
			set_path(case, PROJECT_PATH)
		else:
			style_print(FAIL, ">>> '{}' is not a valid path".format(av[2]))
			select_path()
		return 2
	return 3

def setup():
	global TESTS, TITLE, DIRS
	TESTS, TITLE, DIRS = {}, "", []
	if os.path.isfile(TEST_CASE):
		with open(TEST_CASE, 'r') as f:
			TESTS = json.loads(f.read())
			TITLE = TESTS["TITLE"]
			DIRS = TESTS['directories']
	globals()['dir_test_suite'] = wait_for_results(dir_test_suite, "DIRECTORY TESTS")
	globals()['import_test_suite'] = wait_for_results(import_test_suite, "IMPORT TESTS")
	globals()['class_test_suite'] = wait_for_results(class_test_suite, "CLASS TESTS")
	globals()['ivar_test_suite'] = wait_for_results(ivar_test_suite, "INSTANCE VARIABLE TESTS")
	globals()['imethod_test_suite'] = wait_for_results(imethod_test_suite, "INSTANCE METHOD TESTS")
	globals()['cvar_test_suite'] = wait_for_results(cvar_test_suite, "CLASS VARIABLE TESTS")
	globals()['cmethod_test_suite'] = wait_for_results(cmethod_test_suite, "CLASS METHOD TESTS")
	globals()['func_test_suite'] = wait_for_results(func_test_suite, "FUNCTION TESTS")
	globals()['main_test_suite'] = wait_for_results(main_test_suite, "MAIN TESTS")

def copy_files(src=None, dst=None):
	if src == None:
		src = PROJECT_PATH
	if dst == None:
		dst = '.'
	if not os.path.isdir(dst):
		os.makedirs(dst)
	names = os.listdir(src)
	errors = []
	for name in [x for x in names if x not in BLACKLIST]:
		srcname = src + '/' + name
		dstname = dst + '/' + name
		try:
			if os.path.isdir(srcname):
				copy_files(srcname, dstname)
			else:
				shutil.copy2(srcname, dstname)
		except OSError as why:
			errors.append((srcname, dstname, str(why)))
		except Exception as err:
			errors.extend(err.args[0])
		if errors:
			raise Exception(errors)

def cleanup():
	names = os.listdir('.')
	for name in [x for x in names if x not in WHITELIST]:
		if os.path.isdir(name):
			shutil.rmtree(name)
		else:
			try:
				os.remove(name)
			except:
				pass

#########################################################
#                      TEST HELPERS                     #
#########################################################

def count_handler(var, bool, prereq):
	var[0] += 1 if bool else 0
	var[1] += 1
	var[2] += 0 if prereq else 1
	return bool

def to_class_name(module):
	return module.replace('_', ' ').title().replace(' ', '')

def make_objects(a):
	objs = a.count("make_object")
	while objs > 0:
		i = a.index("make_object")
		c = a[i + 1]
		try:
			end = a.index("make_object", i + 1)
		except:
			end = len(a)
		a[i] = eval(c)(*a[i+2:end])
		del a[i+1:end]
		objs -= 1

def get_vars(test):
	d = {}
	d['args'] = test['args'] if 'args' in test else []
	d['bools'] = reduce(AND, [eval(x) for x in test['prereq_bool']]) if 'prereq_bool' in test else True
	d['cf'] = eval(test['callback_func']) if 'callback_func' in test else None
	d['cm'] = test['callback_msg'] if 'callback_msg' in test else None
	d['ca'] = test['callback_args'] if 'callback_args' in test else []
	d['f'] = test['function'] if 'function' in test else None
	d['ip'] = test["init_params"] if "init_params" in test else []
	d['var'] = test['var'] if 'var' in test else None
	d['class'] = test['class'] if 'class' in test else None
	d['module'] = test['module'] if 'module' in test else None
	d['method'] = test['method'] if 'method' in test else None
	d['directory'] = test['directory'] if 'directory' in test else '.'
	d['namespace'] = test['namespace'] if 'namespace' in test else None
	return d

def run_test(f, *args, **kwargs):
	if kwargs:
		bools = kwargs['prereq_bool'] if 'prereq_bool' in kwargs else []
		ip = kwargs['init_params'] if 'init_params' in kwargs else []
		cf = kwargs['callback_func'] if 'callback_func' in kwargs else None
		cm = kwargs['callback_msg'] if 'callback_msg' in kwargs else None
		ca = kwargs['callback_args'] if 'callback_args' in kwargs else None
	if cf and cm and ca:
		f(*args, prereq_bool=bools, init_params=ip, callback_func=cf, callback_msg=cm, callback_args=ca)
	elif cf and cm:
		f(*args, prereq_bool=bools, init_params=ip, callback_func=cf, callback_msg=cm)
	elif cf and ca:
		f(*args, prereq_bool=bools, init_params=ip, callback_func=cf, callback_args=ca)
	elif cf:
		f(*args, prereq_bool=bools, init_params=ip, callback_func=cf)
	else:
		f(*args, prereq_bool=bools, init_params=ip)

def generic_suite(key):
	tests = TESTS[key] if key in TESTS else []
	for test in tests:
		d = get_vars(test)
		a, b, cf, cm, ca, f, ip = d['args'], d['bools'], d['cf'], d['cm'], d['ca'], d['f'], d['ip']
		var, cn, mt, dy, ns = d['var'], d['class'], d['method'], d['directory'], d['namespace']
		make_objects(a)
		func = eval(key + "_test")
		a = [func] + ([cn] if key == 'class' else (
				[cn, mt] if 'method' in key else (
					[cn, var] if 'var' in key else (
						[f] if 'func' in key else [dy]
					)
				)
			)
		) + a + ([ns] if 'func' in key else [])
		run_test(*a, prereq_bool=b, init_params=ip, callback_func=cf, callback_msg=cm, callback_args=ca)

#########################################################
#                   CALLBACK HANDLERS                   #
#########################################################

def callback_handler(kwargs):
	with suppress_stdout():
		if kwargs != None and 'callback_func' in kwargs:
			if 'callback_args' in kwargs:
				return kwargs['callback_func'](*kwargs['callback_args'])
			else:
				return kwargs['callback_func']()
	return SUCCESS

def thing_do_callback(thing, kwargs, s):
	if callback_handler(kwargs):
		print_ok_subheader(s.format(thing))
		return SUCCESS
	else:
		if kwargs != None and 'callback_msg' in kwargs:
			print_fail_subheader("{}: ".format(thing) + kwargs['callback_msg'])
		else:
			print_fail_subheader("Failed Callback Function")
		return FAILURE

def subthing_do_callback(class_name, thing, kwargs, s):
	if callback_handler(kwargs):
		print_ok_subheader(s.format(class_name, thing))
		return SUCCESS
	else:
		if kwargs != None and 'callback_msg' in kwargs:
			print_fail_subheader("{}.{}: ".format(class_name, thing) + kwargs['callback_msg'])
		else:
			print_fail_subheader("Failed Callback Function")
		return FAILURE

def main_do_callback(main, kwargs):
	return thing_do_callback(main, kwargs, MAIN_OK_STR)

def func_do_callback(function, kwargs):
	return thing_do_callback(function, kwargs, FUNC_OK_STR)

def class_do_callback(class_name, kwargs):
	return thing_do_callback(class_name, kwargs, CLASS_OK_STR)

def method_var_do_callback(class_name, thing, kwargs):
	return subthing_do_callback(class_name, thing, kwargs, ATTR_OK_STR)

#########################################################
#                     TEST FRAMEWORK                    #
#########################################################

def dir_test(path):
	var = "{}_directory_success".format(path)
	globals()[var] = FAILURE
	try:
		p = "./{}".format(path)
		if os.path.isdir(path):
			sys.path.insert(0, p)
			globals()[var] = True
			print_ok_subheader(DIR_STR.format(path))
		else:
			raise Exception
	except:
		print_fail_subheader(DIR_NFOUND_STR.format(path))
	return count_handler(DTESTS, globals()[var], True)

def import_test(module, *imports, prereq_bool=True, **kwargs):
	if kwargs and 'directory' in kwargs:
		d_ = kwargs['directory'] + '_'
		dper = kwargs['directory'] + '.'
		dsl = kwargs['directory'] + '/'
	else:
		d_ = dper = dsl = ''
	var = d_ + module + "_import_success"
	globals()[var] = FAILURE
	if not prereq_bool:
		print_warn_subheader(PREREQ_STR.format(dper+module))
		return count_handler(ITESTS, FAILURE, prereq_bool)
	if(not os.path.isfile("{}{}.py".format(dsl, module))):
		print_fail_subheader(FILE_ERR_STR.format(dsl+module))
		return count_handler(ITESTS, FAILURE, prereq_bool)
	else:
		try:
			exec("import {}{}".format(dper, module))
			globals()[dper + module] = eval(dper + module)
		except Exception as e:
			print_fail_subheader(IMPORT_ERR_STR.format(dsl+module, e))
			return count_handler(ITESTS, FAILURE, prereq_bool)
	for i in imports:
		command = "from {}{} import {}".format(dper, module, i)
		try:
			exec(command)
			globals()[i] = eval(i)
		except:
			print_fail_subheader(NDEF_ERR_STR.format(module, i))
			return count_handler(ITESTS, FAILURE, prereq_bool)
	print_ok_subheader(MODULE_OK_STR.format(module))
	globals()[var] = SUCCESS
	return count_handler(ITESTS, globals()[var], prereq_bool)

def class_test(class_name, *args, prereq_bool=True, **kwargs):
	if kwargs and 'directory' in kwargs:
		d_ = kwargs['directory'] + '_'
	else:
		d_ = ''
	var = d_ + class_name.lower() + "_class_success"
	globals()[var] = FAILURE
	if not prereq_bool:
		print_warn_subheader(PREREQ_STR.format(class_name))
		return count_handler(CTESTS, FAILURE, prereq_bool)
	try:
		with suppress_stdout():
			c = eval(class_name)(*args)
	except Exception as e:
		e = str(e)
		if "0 positional arguments" in e:
			print_fail_subheader(INIT_SELF_ERR_STR.format(class_name))
		elif 'missing' in e and 'argument' in e:
			i = e.index('missing') + 8
			j = e.index(' ', i)
			num = e[i:j]
			print_fail_subheader(INIT_ARG_ERR_STR.format(class_name, num))
		else:
			print_fail_subheader(INIT_ERR_STR.format(class_name, e))
		return count_handler(CTESTS, FAILURE, prereq_bool)
	globals()[var] = class_do_callback(class_name, kwargs)
	return count_handler(CTESTS, globals()[var], prereq_bool)

def imethod_test(class_name, method, *args, prereq_bool=True, **kwargs):
	if kwargs and 'directory' in kwargs:
		d_ = kwargs['directory'] + '_'
	else:
		d_ = ''
	var = "{}{}_{}{}".format(d_, class_name.lower(), method, "_imethod_success")
	globals()[var] = FAILURE
	if not prereq_bool:
		print_warn_subheader(PREREQ_STR.format("{}.{}".format(class_name, method)))
		return count_handler(IMTESTS, FAILURE, prereq_bool)
	with suppress_stdout():
		if kwargs and 'init_params' in kwargs:
			c = eval(class_name)(*kwargs['init_params'])
		else:
			c = eval(class_name)()
	try:
		m = getattr(c, method)
	except:
		print_fail_subheader(NO_MTHD_ERR_STR.format(class_name, method))
		return count_handler(IMTESTS, FAILURE, prereq_bool)
	try:
		with suppress_stdout():
			m(*args)
	except TypeError as e:
		args_str = ', '.join([STR(x) for x in args])
		print_fail_subheader(MTHD_ARGS_ERR_STR.format(class_name, method, args_str))
		print_fail_subheader(str(e))
		return count_handler(IMTESTS, FAILURE, prereq_bool)
	globals()[var] = method_var_do_callback(class_name, method, kwargs)
	return count_handler(IMTESTS, globals()[var], prereq_bool)

def ivar_test(class_name, variable, prereq_bool=True, **kwargs):
	if kwargs and 'directory' in kwargs:
		d_ = kwargs['directory'] + '_'
	else:
		d_ = ''
	var = "{}{}_{}{}".format(d_, class_name.lower(), variable, "_ivar_success")
	globals()[var] = FAILURE
	if not prereq_bool:
		print_warn_subheader(PREREQ_STR.format("{}.{}".format(class_name, variable)))
		return count_handler(IVTESTS, FAILURE, prereq_bool)
	with suppress_stdout():
		if kwargs and 'init_params' in kwargs:
			c = eval(class_name)(*kwargs['init_params'])
		else:
			c = eval(class_name)()
	try:
		getattr(c, variable)
		try:
			with suppress_stdout():
				getattr(eval(class_name), variable)
			print_fail_subheader(IVAR_IS_CVAR_ERR_STR.format(class_name, variable))
			return count_handler(IVTESTS, FAILURE, prereq_bool)
		except:
			pass
	except:
		print_fail_subheader(NO_IVAR_ERR_STR.format(variable, class_name))
		return count_handler(IVTESTS, FAILURE, prereq_bool)
	globals()[var] = method_var_do_callback(class_name, variable, kwargs)
	return count_handler(IVTESTS, globals()[var], prereq_bool)

def cmethod_test(class_name, method, *args, prereq_bool=True, **kwargs):
	if kwargs and 'directory' in kwargs:
		d_ = kwargs['directory'] + '_'
	else:
		d_ = ''
	var = "{}{}_{}{}".format(d_, class_name.lower(), method, "_cmethod_success")
	globals()[var] = FAILURE
	if not prereq_bool:
		print_warn_subheader(PREREQ_STR.format("{}.{}".format(class_name, method)))
		return count_handler(CMTESTS, FAILURE, prereq_bool)
	c = eval(class_name)
	try:
		m = getattr(c, method)
	except:
		print_fail_subheader(NO_MTHD_ERR_STR.format(class_name, method))
		return count_handler(CMTESTS, FAILURE, prereq_bool)
	if not ((inspect.ismethod(m)) and (m.__self__ is c)):
		print_fail_subheader(CMTHD_ERR_STR.format(class_name, method))
		return count_handler(CMTESTS, FAILURE, prereq_bool)
	try:
		with suppress_stdout():
			m(*args)
	except TypeError as e:
		args_str = ', '.join([STR(x) for x in args])
		print_fail_subheader(MTHD_ARGS_ERR_STR.format(class_name, method, args_str))
		print_fail_subheader(str(e))
		return count_handler(CMTESTS, FAILURE, prereq_bool)
	globals()[var] = method_var_do_callback(class_name, method, kwargs)
	return count_handler(CMTESTS, globals()[var], prereq_bool)

def cvar_test(class_name, variable, prereq_bool=True, **kwargs):
	if kwargs and 'directory' in kwargs:
		d_ = kwargs['directory'] + '_'
	else:
		d_ = ''
	var = "{}{}_{}{}".format(d_, class_name.lower().replace('.', '_'), variable, "_cvar_success")
	globals()[var] = FAILURE
	if not prereq_bool:
		print_warn_subheader(PREREQ_STR.format("{}.{}".format(class_name, variable)))
		return count_handler(CVTESTS, FAILURE, prereq_bool)
	c = eval(class_name)
	try:
		getattr(c, variable)
	except:
		print_fail_subheader(NO_CVAR_ERR_STR.format(variable, class_name))
		return count_handler(CVTESTS, FAILURE, prereq_bool)
	globals()[var] = method_var_do_callback(class_name, variable, kwargs)
	return count_handler(CVTESTS, globals()[var], prereq_bool)

def function_test(function, *args, namespace="", prereq_bool=True, **kwargs):
	var = "{}{}".format(function.replace('.', '_'), "_function_success")
	globals()[var] = FAILURE
	if not prereq_bool:
		print_warn_subheader(PREREQ_STR.format(function))
		return count_handler(FTESTS, FAILURE, prereq_bool)
	try:
		with suppress_stdout():
			if namespace:
				f = eval("{}.{}".format(namespace, function))
			else:
				f = eval(function)
	except:
		print_fail_subheader(NO_FUNC_ERR_STR.format(function))
		return count_handler(FTESTS, FAILURE, prereq_bool)
	try:
		with suppress_stdout():
			f(*args)
	except TypeError as e:
		print_fail_subheader(FUNC_ARGS_ERR_STR.format(function, ', '.join([str(x) for x in args])))
		print_fail_subheader(str(e))
		return count_handler(FTESTS, FAILURE, prereq_bool)
	except Exception as e:
		print_fail_subheader(FUNC_ERR_STR.format(function, e))
		return count_handler(FTESTS, FAILURE, prereq_bool)
	globals()[var] = func_do_callback(function, kwargs)
	return count_handler(FTESTS, globals()[var], prereq_bool)

def main_test(directory, *args, prereq_bool=True, **kwargs):
	dper = (directory + ".") if directory else ""
	d_ = (directory.replace('.', '_') + "_") if directory else ""
	var = "{}main_success".format(d_)
	globals()[var] = FAILURE
	if not prereq_bool:
		print_warn_subheader(PREREQ_STR.format(directory + "/main"))
		return count_handler(MTESTS, FAILURE, prereq_bool)
	command = "from {}main import main as {}main".format(dper, d_)
	try:
		exec(command)
	except Exception as e:
		print_fail_subheader(MAIN_ERR_STR.format(d_, e))
		return count_handler(MTESTS, globals()[var], prereq_bool)
	try:
		with suppress_stdout():
			eval("{}main".format(d_))(*args)
	except Exception as e:
		e = str(e)
		print_fail_subheader(MAIN_ERR_STR.format(d_, e))
		return count_handler(MTESTS, globals()[var], prereq_bool)
	globals()[var] = main_do_callback("{}main".format(d_), kwargs)
	return count_handler(MTESTS, globals()[var], prereq_bool)

#########################################################
#                      TEST SUITES                      #
#########################################################

def dir_test_suite():
	for d in DIRS:
		dir_test(d)

def import_test_suite():
	tests = TESTS['import'] if 'import' in TESTS else []
	for test in tests:
		m = test["module"]
		thing = test["import"]
		dy = test["directory"]
		b = reduce(AND, [eval(x) for x in test['prereq_bool']]) if 'prereq_bool' in test else True
		import_test(m, thing, prereq_bool=b, directory=dy)

def class_test_suite():
	generic_suite('class')

def imethod_test_suite():
	generic_suite('imethod')

def ivar_test_suite():
	generic_suite('ivar')
	
def cvar_test_suite():
	generic_suite('cvar')

def cmethod_test_suite():
	generic_suite('cmethod')

def func_test_suite():
	generic_suite('function')

def main_test_suite():
	generic_suite('main')

#########################################################
#                          MAIN                         #
#########################################################

def main():
	result = handle_cli()
	if result == 3:
		select_project()
		select_path()
	elif result == 4:
		select_path()
	if result != 1:
		copy_files()
		setup()
		print_begin()
		dir_test_suite()
		import_test_suite()
		class_test_suite()
		ivar_test_suite()
		cvar_test_suite()
		imethod_test_suite()
		cmethod_test_suite()
		func_test_suite()
		main_test_suite()
		print_results()
		print_end()
		cleanup()

if __name__ == "__main__":
	main()
